<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'diapo'=>'Diaporama',
'diapo_config'=>'Configuration du plugin Diapo',
'nombre_vignettes'=>'nombre de vignettes par page',
'nombre_vignettes_explication'=>'par défaut 15',
'taille_diapo_grand'=>'taille maximum de la grande image',
'taille_diapo_grand_explication'=>'diaporama et centré, par défaut 500',
'taille_diapo_petit'=>'taille maximum de la petite image',
'taille_diapo_petit_explication'=>'avec vignettes sur le côté, par défaut 300',
'taille_vignettes'=>'taille des vignettes',
'taille_vignettes_explication'=>'par défaut 60',
'temps_pause'=>'temps de pause du diaporama',
'temps_pause_explication'=>'par défaut 5000',
'txt_icodiapo'=>'Diaporama lecture/pause',
'txt_icoleft'=>'Vignettes &agrave; gauche',
'txt_icoright'=>'Vignettes &agrave; droite',
'txt_icocenter'=>'Vignettes en haut (grande image)',
'ico_diapo_play'=>'&gt;',
'ico_diapo_pause'=>'||',
'ico_left'=>'&nbsp;',
'ico_right'=>'&nbsp;',
'ico_center'=>'&nbsp;',
'loading'=>'Chargement en cours...'
);